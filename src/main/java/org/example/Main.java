package org.example;

import org.example.service.CashbackService;

public class Main {
    public static void main(String[] args) {
        CashbackService service = new CashbackService();
        System.out.println(service.calculate(389_76));
    }
}
